function cable(x, y) {

  let left;
  let right;

  row = {
      "1":"13",
      "2":"44",
      "3":"76",
      "4":"108",
      "5":"140",
      "6":"172",
      "7":"204",
      "8":"236",
      "9":"268",
      "shield":"300"};
  left = row[x];
  right = row[y];

 


  return `<polyline points="53,${left}  73,${left}  257,${right}  272,${right}"  id="DB-9_TxD-3" stroke-dasharray="2" />`
}

// Grab all the elements
const button = document.querySelector('.submit');
const svg = document.querySelector('svg');
const x = document.querySelector('.x');
const y = document.querySelector('.y');

// Add an event listener to the button
button.addEventListener('click', addShape, false);

function addShape(e) {

  // Prevent the form from submitting
  e.preventDefault();
  svg.insertAdjacentHTML('beforeend', cable(x.value, y.value));
}


let fff = document.getElementById('mydiv');
fff.children[0].onchange = function(){
    fff.style.backgroundColor = fff.children[0].value;
};

