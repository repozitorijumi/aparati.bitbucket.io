const debounce = (func) => {
  let timer
  return (event) => {
    if (timer) { clearTimeout(timer) }
    timer = setTimeout(func, 100, event)
  }
}

window.addEventListener('resize', debounce(() => {
  canvas.width = window.innerWidth
  canvas.height = window.innerHeight
}))



var c = document.getElementById("myCanvas");
var ctx1 = c.getContext("2d");

ctx1.beginPath();
ctx1.strokeStyle = "#AAAAAA";
ctx1.lineWidth = 3;
ctx1.lineJoin = "round";
ctx1.moveTo(10, 10);
ctx1.lineTo(50, 10);
ctx1.lineTo(200, 270);
ctx1.lineTo(240, 270);
ctx1.lineTo(230, 268);
ctx1.lineTo(230, 273);
ctx1.lineTo(240, 270);
ctx1.stroke();

var ctx2 = c.getContext("2d");
ctx2.strokeStyle = "#000";
ctx2.beginPath();
ctx2.lineWidth = 3;
ctx2.lineJoin = "round";
ctx2.moveTo(10, 40);
ctx2.lineTo(110, 40);
ctx2.lineTo(150, 70);
ctx2.lineTo(240, 70);
ctx2.lineTo(230, 67);
ctx2.lineTo(230, 73);
ctx2.lineTo(240, 70);
ctx2.stroke();

var ctx3 = c.getContext("2d");
ctx3.beginPath();
ctx3.lineWidth = 3;
ctx3.lineJoin = "round";
ctx3.moveTo(10, 70);
ctx3.lineTo(20, 70);
ctx3.lineTo(20, 67);
ctx3.lineTo(10, 70);
ctx3.lineTo(20, 73);
ctx3.lineTo(20, 70);
ctx3.lineTo(110, 70);
ctx3.lineTo(150, 40);
ctx3.lineTo(240, 40);
ctx3.stroke();
